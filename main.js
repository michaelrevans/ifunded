/**
 * Created by mevans on 03.07.17.
 */
var scrollButton = $('.headerBunner__textCont .cta-button');
var introSection = $('.intro');

scrollButton.on('click', function() {
    $('html, body').animate({
        scrollTop: introSection.offset().top
    }, 800);
});